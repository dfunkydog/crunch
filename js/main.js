jQuery(document).ready(function($) {
	// might be a good idea to set initial date as today
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yy = today.getFullYear().toString().substr(2);
	if(dd<10){dd="0"+dd;}
	if(mm<10){mm="0"+mm;}
	$("input#mDate").val(dd+"/"+mm+"/"+yy);



// Display Vat rate calculations on change
$('#line-items-input').on('change', '.type-of-expense select', function() {
	var vatRate;
	var amt = $(this).parent().siblings('.amount').find('input').val();
	var breakDown = {};
	var ele = $(this).parent().siblings('.breakdown');

	if ($('option:selected', this).data('vat') === "yes") {
		$('.vat-rate').text("20% Vat");
		vatRate = 20;
	} else {
		$('.vat-rate').text("No VAT");
		vatRate = 0;
	}
	breakDown = calculateVat(vatRate,amt,ele);

});

function calculateVat(rate, amt,line){
	rate = typeof parseInt(rate,10) !== 'undefined' ? rate : parseInt(20,10);
	rate = ( rate + 100) / 100;
	var netFig = amt / rate;
	var vatFig = amt-netFig;
	line.find('.line-vat').text("£"+vatFig.toFixed(2));
	line.find('.line-net').text("£"+netFig.toFixed(2));
}


// validate & calculate on key up
$( '#line-items-input' ).on('keyup',".amount input", function() {
	var ele = $(this).parent().siblings('.breakdown');
	var valAmt = validateEntry($(this).val());
	if (!valAmt) {
		ele.find('.line-vat').text("Please enter a valid amount");
		return;
	}
	var vatRate;
	var amt = $(this).val();
	var breakDown = {};
	var expType = $(this).parent().siblings('.type-of-expense');

	if (expType.find('option:selected').data('vat') === "yes") {
		vatRate = 20;
	} else vatRate = 0;

	breakDown = calculateVat(vatRate,amt,ele);

}).keyup();

function validateEntry(entry) {
	var res = /^([0-9]+)(\.[0-9]{0,2})?$/.test(entry);
   if (res) {return true;} else {return false;}
}



// format form data

function formHandler(form){
	var formValues = $(form).serializeArray();
// make sure data is fresh
	$.getJSON('expenses.json', function(expenses) {
			}).done(function(expenses){
				var idArray = [];
				$.each(expenses, function(index, val) {
					 idArray.push(parseInt(val.ID),10);
				});

		var newId = Math.max.apply(Math, idArray);
			newId++;
			newId = {"name":"ID","value": newId.toString()};
			formValues.unshift(newId);
			$.each(formValues,function(i, vt){
				if(vt.name === "expType"){
					if(vt.value == "local-travel" || vt.value == "overseas-travel"){
						formValues.push({"name":"vRate", "value":"0"});
					} else {
						formValues.push({"name":"vRate", "value":"20"});
					}
				}
			});
			var newValues = {};
			$.each(formValues, function() {
				newValues[this.name] = this.value || '';
			});

			$.post('update.php',newValues, function(response) {
						//server error messages perhaps
					$(form)[0].reset();
					$("input#mDate").val(dd+"/"+mm+"/"+yy);
			}).fail(function(){
				console.log("error");
			}).done(function(){
				$.getJSON('expenses.json', function(expenses) {
					createTable(expenses);
				});
			});
		});
}

function valueToNicename(fname){
  var nicename;
  if(fname == "telephone"){ nicename = "Telephone";}
  else if(fname == "telephone"){ nicename = "Telephone";}
  else if(fname == "local-travel"){ nicename = "Public Transport & Taxis";}
  else if(fname == "computer"){ nicename = "Computer Consumables";}
  else if(fname == "substinence"){ nicename = "Substinence";}
  else if(fname == "overseas-travel"){ nicename = "Overseas Travel";}
  return nicename;
}

function createTable (exp) {
			var tr;
			for (var i = 0; i < exp.length; i++) {
				tr = $('<tr/>');
				var vat = vatCalc(exp[i].vRate,exp[i].amount);
				tr.append("<td class='deleterow'>del</td>");
				tr.append("<td class='gdate'>"+ exp[i].mDate +"</td>");
				tr.append("<td class='gexptype'>"+ valueToNicename(exp[i].expType) + "</td>");
				tr.append("<td class='grate'>"+ exp[i].vRate + "%</td>");
				tr.append("<td class='gdescription'>"+ exp[i].description + "</td>");
				tr.append("<td>£"+ vat.vatFig + "</td>");
				tr.append("<td>£"+ vat.netFig + "</td>");
				tr.append("<td class='gamount'>"+ parseInt(exp[i].amount, 10).toFixed(2) + "</td>");
				tr.append("<td class='gId'>"+ exp[i].ID+ "</td>");
				tr.append("<td class='editrow'>edit</td>");
				$('table#expenses').append(tr);
			}
		}

$("form#form").submit(function(e) {
	formHandler($(this));
	e.preventDefault();
});

// post form

function vatCalc(rate, amt){
	rate = typeof parseInt(rate,10) !== 'undefined' ? rate : parseInt(20,10);
	rate = ( rate + 100) / 100;
	var netFig = amt / rate;
	var vatFig = amt-netFig;
	return {
		vatFig:vatFig.toFixed(2),
		netFig:netFig.toFixed(2)
	};
}
////  Edit row

$("table#expenses").on('click','.editrow', function(event) {
	// get existing values
	var trDate = $(this).siblings('.gdate').text();
	var trexpType = $(this).siblings('.gexptype').text();
	var trDescription = $(this).siblings('.gdescription').val();
	var trAmount = $(this).siblings('.gamount').text();
	// fill form with existing for values
	$('#mDate').val(trDate);
	$("select option[value="+trexpType+"]").prop('selected',true);
	$('#description').val(trDescription);
	$('#amount').val(trAmount);
	console.log(trDate,trDescription,trexpType,trAmount);
});


///// delete row

$("table#expenses").on('click','.deleterow', function(event) {
	event.preventDefault();
	var rowIdnum = $(this).siblings('.gId').text();
	var thisRow = $(this).parent("tr");
	var desId = {"ID" : rowIdnum }; //get the data get id on &serialze the
	$.post('delete.php',desId, function(response) {

			}).fail(function(){
				alert("Sorry, We could not to remove this row");
			}).done(function(){
				thisRow.fadeOut(function(){
					thisRow.remove();
				});
			});

});

});