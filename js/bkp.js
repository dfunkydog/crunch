// JSON operations
var expenses;
// get latest expense data
function fetchExpenses(){
	$.getJSON('expenses.json', function(expenses) {
		createTable(expenses);

}).fail(function(){
	alert ("Couldn't retieve expenses list");
	}).done(function(){
		return expenses;
	});
}
//create

function formHandler(form){
	var formValues = $(form).serializeArray();
	$.getJSON('expenses.json', function(expenses) {
		var newId = expenses.length+1;
		newId = {"name":"ID","value": newId.toString()};
		formValues.unshift(newId);
		$.each(formValues,function(i, vt){
			if(vt.name === "expType"){
				if(vt.value == "local-travel" || vt.value == "overseas-travel"){
					formValues.push({"name":"vRate", "value":"0"});
				} else {
					formValues.push({"name":"vRate", "value":"20"});
				}
			}
		});
		var newValues = {};
		$.each(formValues, function() {
			newValues[this.name] = this.value || '';
		});
		expenses.push(newValues);

		// Create Table
		function createTable (exp) {
			var tr;
			for (var i = 0; i < exp.length; i++) {
				var tr = $('<tr/>');
				var vat = vatCalc(exp[i].vRate,exp[i].amount);
				tr.append("<td>"+ exp[i].mDate +"</td>");
				tr.append("<td>"+ exp[i].expType + "</td>");
				tr.append("<td>"+ exp[i].vRate + "%</td>");
				tr.append("<td>"+ exp[i].description + "</td>");
				tr.append("<td>£"+ vat.vatFig + "</td>");
				tr.append("<td>£"+ vat.netFig + "</td>");
				tr.append("<td>"+ parseInt(exp[i].amount, 10).toFixed(2) + "</td>");
				$('table#expenses').append(tr);
			};
		}

		pdata = JSON.stringify(expenses);
			console.log(pdata);
		$.post('update.php', function(pdata) {
		}).fail(function(){console.log("fail");})

		createTable(expenses);

	});
}

$("form#form").submit(function(e) {
	formHandler($(this));
	e.preventDefault();
});



function vatCalc(rate, amt){
	rate = typeof parseInt(rate,10) !== 'undefined' ? rate : parseInt(20,10);
	rate = ( rate + 100) / 100;
	var netFig = amt / rate;
	var vatFig = amt-netFig;
	return {
		vatFig:vatFig.toFixed(2),
		netFig:netFig.toFixed(2)
	}

}
